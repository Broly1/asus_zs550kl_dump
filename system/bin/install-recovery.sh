#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:27954463:15d95429bc7c32ce9ac92d649433c45836882c47; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:25363739:ea4ca1a13ff73019a844334c37b81e802ed121d2 EMMC:/dev/block/bootdevice/by-name/recovery 15d95429bc7c32ce9ac92d649433c45836882c47 27954463 ea4ca1a13ff73019a844334c37b81e802ed121d2:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
