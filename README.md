# Z018-user 8.0.0 OPR1.170623.026 WW_user_80.30.123.118_20181102 release-keys
Z018-user 8.0.0 OPR1.170623.026 WW_user_80.30.123.118_20181102 release-keys
Z018-user 8.0.0 OPR1.170623.026 WW_user_80.30.123.118_20181102 release-keys
- manufacturer: asusasusasus
- platform: msm8953msm8953msm8953
- codename: ASUS_Z018_1
- flavor: Z018-user
Z018-user
Z018-user
- release: 8.0.0
8.0.0
8.0.0
- id: OPR1.170623.026
OPR1.170623.026
OPR1.170623.026
- incremental: WW_80.30.123.118_20181102
WW_80.30.123.118_20181102
WW_80.30.123.118_20181102
- tags: release-keys
release-keys
release-keys
- fingerprint: asus/WW_Z018/ASUS_Z018_1:8.0.0/OPR1.170623.026/WW_80.30.123.118_20181102:user/release-keys
asus/WW_Z018/ASUS_Z018_1:8.0.0/OPR1.170623.026/WW_80.30.123.118_20181102:user/release-keys
asus/WW_Z01F/ASUS_Z01F_1:8.0.0/OPR1.170623.026/WW_80.30.123.118_20181102:user/release-keys
- is_ab: false
- brand: asus
- branch: Z018-user-8.0.0-OPR1.170623.026-WW_user_80.30.123.118_20181102-release-keys
Z018-user-8.0.0-OPR1.170623.026-WW_user_80.30.123.118_20181102-release-keys
Z018-user-8.0.0-OPR1.170623.026-WW_user_80.30.123.118_20181102-release-keys
- repo: asus_asus_z018_1_dump
